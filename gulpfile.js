var gulp = require('gulp'),
sass = require('gulp-sass'),
browserSync = require('browser-sync'),
autoprefixer = require('gulp-autoprefixer'),
bourbon = require('node-bourbon'),
notify = require("gulp-notify"),

pxtorem = require('gulp-pxtorem'),
sourcemaps = require('gulp-sourcemaps');
var postcss = require('gulp-postcss');
// var pxtorem = require('postcss-pxtorem');



// gulp.task('css', function () {

//     var processors = [
//         autoprefixer({
//             browsers: 'last 1 version'
//         }),
//         pxtorem({
//             replace: false
//         })
//     ];

//     return gulp.src(['build/css/**/*.css'])
//         .pipe(postcss(processors))
//         .pipe(gulp.dest('build/css'));
// });




gulp.task('browser-sync', function() {
	browserSync({
		server: {
			// baseDir: 'elektrozavod'
		},
		// tunnel: true
		notify: false,
		directory: true
	});

});



gulp.task('sass', function () {
  return gulp.src('scss/*.+(scss|sass)') // Выборка исходных файлов для обработки плагином  
  .pipe(sourcemaps.init())   // Для генераии style.css.map
  .pipe(sass({includePaths: bourbon.includePaths}).on("error", notify.onError()))  
    .pipe(autoprefixer(['last 100 versions', '> 10%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы    
    .pipe(pxtorem({
      replace: false,         
      propList: ['font', 'font-size', 'line-height', 'letter-spacing', 'padding-left', 'padding-right', 'padding-bottom', 'padding-top', 'padding', 'margin', 'margin-left', 'margin-right', 'margin-bottom', 'margin-top', 'width', 'max-width', 'min-width', 'max-height','min-height', 'height', 'top', 'left', 'right', 'bottom'],  
      selectorBlackList: [/^html$/],    
      mediaQuery: true,
    }))
    .pipe(sourcemaps.write('./')) // Куда положить .map
    .pipe(gulp.dest('css')) // Вывод результирующего файла в папку назначения (dest - пункт назначения)
    .pipe(browserSync.reload({stream: true}))
  });


gulp.task('watch', ['sass','browser-sync'], function() {
	gulp.watch('scss/**/*.+(scss|sass)', ['sass']); 
	gulp.watch('*.html', browserSync.reload); 
	gulp.watch('js/**/*.js', browserSync.reload); 
});






gulp.task('default', ['watch']);