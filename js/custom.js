$(window).load(function() {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
    $('body').addClass('ios');
  } else {
    $('body').addClass('web');
  };
  $('body').removeClass('loaded');
});
/* viewport width */
function viewport() {
  var e = window,
    a = 'inner';
  if (!('innerWidth' in window)) {
    a = 'client';
    e = document.documentElement || document.body;
  }
  return {
    width: e[a + 'Width'],
    height: e[a + 'Height']
  }
};

setTimeout(function() {
  // $('.box-btn-slide--screen3').removeClass('fadeOutUp fadeOutDown fadeInUp fadeInDown animated');    
  // $('.box-btn-slide--screen3').addClass('fadeInUp  animated').css('animation-delay', '2s'); 

}, 200);

/* viewport width */

$(document).ready(function() {
  /* retina images */
  if (window.devicePixelRatio > 1) {
    var lowresImages = $('img.content_img');
    lowresImages.each(function(i) {
      var lowres = $(this).attr('src');
      var highres = lowres.replace(".png", "@2x.png");
      $(this).attr('src', highres);
    });
  }
  //--

  /*печатающий текст*/
  if ($('.js-typed-strings').length) {
    var typed = new Typed(".js-typed", {
      stringsElement: '.js-typed-strings',
      typeSpeed: 60,
      backSpeed: 0,
      backDelay: 200,
      startDelay: 0,
      loop: false,
    });
  };

  if ($('.js-typed-strings2').length) {
    var typed2 = new Typed(".js-typed2", {
      stringsElement: '.js-typed-strings2',
      typeSpeed: 60,
      backSpeed: 0,
      backDelay: 200,
      startDelay: 0,
      loop: false,
    });
  };
  if ($('.js-typed-strings3').length) {
    var typed3 = new Typed(".js-typed3", {
      stringsElement: '.js-typed-strings3',
      typeSpeed: 40,
      backSpeed: 0,
      backDelay: 200,
      startDelay: 500,
      loop: false,
    });
  };
  if ($('.js-typed-strings4').length) {
    var typed4 = new Typed(".js-typed4", {
      stringsElement: '.js-typed-strings4',
      typeSpeed: 40,
      backSpeed: 0,
      backDelay: 200,
      startDelay: 500,
      loop: false,
    });
  };
  /*печатающий текст --*/

  /* Инициализация слайдера */
  var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    direction: 'vertical',
    slidesPerView: 1,
    paginationClickable: true,
    spaceBetween: 0,
    mousewheelControl: true,
    loop: true,
    speed: 1000,
    onSlideChangeStart: function(swiper) {
      if ($('.screen-1').hasClass('swiper-slide-active')) {
        setTimeout(function() {
          if ($('.js-typed-strings').length) {
            typed.reset();
          };
        }, 100);
        $('.screen-1 .box-btn-slide').addClass('fadeInLeft animated').css('animation-delay', '0.2s');
        $('.screen-1 .box-head-block-slide').addClass('fadeInRight animated').css('animation-delay', '0.7s');
        $('.screen-1 .box-img-slide').addClass('fadeInRight animated').css('animation-delay', '0.7s');
        $('.screen-1 .box-two-col-slide').addClass('fadeIn animated').css('animation-delay', '0.7s');
      } else {
        setTimeout(function() {
          if ($('.js-typed-strings').length) { typed.destroy(); };
          $('.screen-1 .box-btn-slide').removeClass('fadeInLeft animated');
          $('.screen-1 .box-head-block-slide').removeClass('fadeInRight animated');
          $('.screen-1 .box-img-slide').removeClass('fadeInRight animated');
          $('.screen-1 .box-two-col-slide').removeClass('fadeIn animated');
        }, 600);

      };

      if ($('.screen-1.swiper-slide-duplicate').hasClass('swiper-slide-active')) {
        setTimeout(function() {
          if ($('.screen-1.swiper-slide-duplicate .js-typed').html().length) {

          } else {
            if ($('.screen-1.swiper-slide-duplicate .js-typed-strings').length) {
              var typedDublicat = new Typed(".screen-1.swiper-slide-duplicate .js-typed", {
                stringsElement: '.screen-1.swiper-slide-duplicate .js-typed-strings',
                typeSpeed: 60,
                backSpeed: 0,
                backDelay: 200,
                startDelay: 0,
                loop: false,
              });
            };
          }
        }, 100);
      } else {
        setTimeout(function() {
          // if($('.swiper-slide-duplicate .js-typed-strings').length) {typedDublicat.destroy();};    
          if ($('.screen-1.swiper-slide-duplicate .js-typed-strings').length) {
            $('.screen-1.swiper-slide-duplicate .js-typed').html("");
            console.log("delete")
          };
        }, 500);
      };

      if ($('.screen-4.swiper-slide-duplicate').hasClass('swiper-slide-active')) {
        setTimeout(function() {
          if ($('.screen-4.swiper-slide-duplicate .js-typed4').html().length) {

          } else {
            if ($('.screen-4.swiper-slide-duplicate .js-typed-strings4').length) {
              var typedDublicat2 = new Typed(".screen-4.swiper-slide-duplicate .js-typed4", {
                stringsElement: '.screen-4.swiper-slide-duplicate .js-typed-strings4',
                typeSpeed: 60,
                backSpeed: 0,
                backDelay: 200,
                startDelay: 0,
                loop: false,
              });
            };
          }
        }, 100);
      } else {
        setTimeout(function() {
          // if($('.swiper-slide-duplicate .js-typed-strings').length) {typedDublicat.destroy();};    
          if ($('.screen-4.swiper-slide-duplicate .js-typed-strings4').length) {
            $('.screen-4.swiper-slide-duplicate .js-typed4').html("");
            console.log("delete")
          };
        }, 500);
      };

      if ($('.screen-2').hasClass('swiper-slide-active')) {
        setTimeout(function() {
          if ($('.js-typed-strings').length) { typed2.reset(); };
        }, 100);
        $('.screen-2 .box-head-block-slide').addClass('fadeInRight animated').css('animation-delay', '0.7s');
        $('.screen-2 .box-carusel').addClass('fadeIn animated').css('animation-delay', '0.9s');
      } else {
        setTimeout(function() {
          if ($('.js-typed-strings').length) { typed2.destroy(); };
          $('.screen-2 .box-head-block-slide').removeClass('fadeInRight animated');
          $('.screen-2 .box-carusel').removeClass('fadeIn animated');
        }, 600);

      };

      if ($('.screen-3').hasClass('swiper-slide-active')) {
        setTimeout(function() {
          if ($('.js-typed-strings3').length) { typed3.reset(); };
        }, 400);
        setTimeout(function() {
          $('.demounted-engine').addClass('active');
        }, 3200);
        $('.box-btn-slide--screen3').addClass('fadeInLeft animated').css('animation-delay', '0.2s');

      } else {
        setTimeout(function() {
          if ($('.js-typed-strings3').length) { typed3.destroy(); };
          $('.demounted-engine').removeClass('active');
          $('.box-btn-slide--screen3').removeClass('fadeInLeft animated');
        }, 600);
      };

      if ($('.screen-4').hasClass('swiper-slide-active')) {

        setTimeout(function() {
          if ($('.js-typed-strings4').length) { typed4.reset(); };
        }, 400);
        $('.screen-4 .box-head-block-slide').addClass('fadeInRight animated').css('animation-delay', '0.7s');
        $('.screen-4 .box-carusel').addClass('fadeIn animated').css('animation-delay', '0.9s');

      } else {
        setTimeout(function() {
          if ($('.js-typed-strings4').length) { typed4.destroy(); };
          $('.screen-4 .box-head-block-slide').removeClass('fadeInRight animated');
          $('.screen-4 .box-carusel').removeClass('fadeIn animated');
        }, 600);

      };
    }
  });
  /* Инициализация слайдера */


  /* Инициализация карусели */
  $('.owl-carousel').owlCarousel({
      loop: true,
      responsiveClass: true,
      autoWidth: true,
      items: 3,
      smartSpeed: 800,
      // center: true,
      responsive: {
        0: {
          items: 1,
          // nav: true
        },
        480: {
          items: 1,
          center: true,
        },
        991: {
          items: 2,
          // nav: false
          center: true,
        },
        1100: {
          items: 3,
          center: true,
          // nav: true,
          // loop: true
        }
      }
    });
    /* Инициализация карусели */

  /* Всплывающее левое меню */

  $(window).resize(function() {
    if ($(window).width() < 992) {
      $('.fix-panel-btn').css("display", "block", 'left', '1.5625rem');
      $('.fix-panel').css("width", "0rem");
    } else {
      $('.fix-panel-btn').css("display", "none", 'left', '1.5625rem');
      $('.fix-panel').css("width", "14.4375rem");
    }
  });
  $('.fix-panel-btn').click(function() {
    if ($(this).hasClass('fix-panel-btn_active')) {
      $(this).removeClass('fix-panel-btn_active');
      $('.fix-panel-btn').animate({ left: '1.5625rem' }, 300).css('transform', 'rotate(0deg)');
      $('.fix-panel').removeClass('fix-panel_overflow').animate({ width: '0rem' }, 300);
    } else {
      $(this).addClass('fix-panel-btn_active');
      $('.fix-panel-btn').animate({ left: '15.5625rem' }, 300).css('transform', 'rotate(180deg)');
      $('.fix-panel').addClass('fix-panel_overflow').animate({ width: '14.4375rem' }, 300);
    }
  });
});

$(function() {
  /* placeholder*/
  $('input, textarea').each(function() {
    var placeholder = $(this).attr('placeholder');
    $(this).focus(function() {
      $(this).attr('placeholder', '');
    });
    $(this).focusout(function() {
      $(this).attr('placeholder', placeholder);
    });
  });
  /* placeholder*/

  $('.button-nav').click(function() {
    $(this).toggleClass('active'),
      $('.main-nav-list').slideToggle();
    return false;
  });

  /* components */




  /*
	
    if($('.styled').length) {
    	$('.styled').styler();
    };
    if($('.fancybox').length) {
    	$('.fancybox').fancybox({
    		margon  : 10,
    		padding  : 10
    	});
    };
    if($('.slick-slider').length) {
    	$('.slick-slider').slick({
    		dots: true,
    		infinite: false,
    		speed: 300,
    		slidesToShow: 4,
    		slidesToScroll: 4,
    		responsive: [
    			{
    			  breakpoint: 1024,
    			  settings: {
    				slidesToShow: 3,
    				slidesToScroll: 3,
    				infinite: true,
    				dots: true
    			  }
    			},
    			{
    			  breakpoint: 600,
    			  settings: "unslick"
    			}				
    		]
    	});
    };
    if($('.scroll').length) {
    	$(".scroll").mCustomScrollbar({
    		axis:"x",
    		theme:"dark-thin",
    		autoExpandScrollbar:true,
    		advanced:{autoExpandHorizontalScroll:true}
    	});
    };
	
    */

  /* components */


  /*Отркываем-закрываем меню*/
  $('.js-btn-menu').click(function() {
    $('html').addClass('_shifted');
  });
  $('.js-btn-menu-close').click(function() {
    $('html').removeClass('_shifted');
  });
  /*Отркываем-закрываем меню*/

});

var handler = function() {

  var height_footer = $('footer').height();
  var height_header = $('header').height();
  //$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});


  var viewport_wid = viewport().width;
  var viewport_height = viewport().height;

  if (viewport_wid <= 991) {

  }

}
$(window).bind('load', handler);
$(window).bind('resize', handler);